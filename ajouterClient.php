<?php
	require_once "includes/bdd.php";

	// Requête SQL ajout client
	$ajouterClient = $bdd->prepare("INSERT INTO biblio_client VALUES (DEFAULT, :nom, :prenom, :sexe, :adresse, :cp, :ville, :tel, :mail)");

	// Tableau informations client
	$infosClient = [];

	// Stockage des infos dans tableau
	foreach($_POST as $key => $value){
		$infosClient[$key] = $value;
	}

	// Exécution de la requête
	$ajouterClient->execute($infosClient);

	// Récupération du nouveau client
	$nouveauClient = $bdd->prepare("SELECT code FROM biblio_client WHERE code = (SELECT MAX(code) FROM biblio_client)");
	$nouveauClient->execute();
	$nouveauClient = $nouveauClient->fetch();
?>

<script type="text/javascript">
	// Redirection vers le nouveau client
	window.location.href = 'client.php?code=<?= $nouveauClient['code'] ?>';
</script>