<?php
	require_once "includes/bdd.php";

	// Récupération des livres
	$livres = $bdd->prepare("
		SELECT ISBN, titre, auteur, nom, stock, stock - (SELECT COUNT(livre) FROM biblio_emprunt WHERE biblio_emprunt.livre = biblio_livre.ISBN)
		FROM biblio_livre
		INNER JOIN biblio_genre ON biblio_livre.genre = biblio_genre.id
	");
	$livres->execute();
	$livres = $livres->fetchAll(PDO::FETCH_ASSOC);
	// var_dump($livres);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Livres</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<ul id="nav">
			<!-- Navigation -->
			<a href=".">Retour à l'accueil</a>
		</ul>
		<hr>
		<h2>Liste des livres</h2>
		<table>
			<tr>
				<th>ISBN</th>
				<th>Titre</th>
				<th>Auteur</th>
				<th>Genre</th>
				<th>Stock</th>
				<th>Disponible</th>
			</tr>
			<?php
			foreach($livres as $livre){
				echo "<tr>";
				foreach($livre as $key => $info){
					echo "<td>".$info."</td>";
				}
				echo "<td><a href=\"livre.php?ISBN=".$livre["ISBN"]."\">Voir/éditer</a></td>";
				echo "</tr>";
			}
			?>
		</table>
	</div>
</body>
</html>