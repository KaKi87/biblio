<?php
	require_once "includes/bdd.php";

	// Suppression du livre
	$livre = $bdd->prepare("DELETE FROM biblio_livre WHERE ISBN = :ISBN");
	$livre->execute([
		"ISBN" => $_GET["ISBN"]
	]);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Livre supprimé</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<p>Livre supprimé.</p>
		<ul class="nav">
			<li><a href="index.php">Retour à l'accueil</a></li>
			<li><a href="livres.php">Retour à la gestion des livres</a></li>
		</ul>
	</div>
</body>
</html>