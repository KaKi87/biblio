<?php
require_once "includes/bdd.php";

// Requête SQL édition genre
$editerGenre = $bdd->prepare("
		UPDATE biblio_genre
		SET
			nom = :nom
		WHERE id = :id
");
$editerGenre->execute([
	"id" => $_POST["id"],
	"nom" => $_POST["nom"]
]);
?>

<script type="text/javascript">
	// Redirection vers page genre
	window.location.href = 'genre.php?id=<?= $_POST['id'] ?>';
</script>