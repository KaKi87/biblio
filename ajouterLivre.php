<?php
	require_once "includes/bdd.php";

	// Requête SQL ajout livre
	$ajouterLivre = $bdd->prepare("INSERT INTO biblio_livre VALUES (:ISBN, :titre, :auteur, :genre, :parution, :mc, :stock, :editeur, :collection, :nbPages)");

	// Tableau informations livre
	$infosLivre = [];

	// Stockage des infos dans tableau
	foreach($_POST as $key => $value){
		$infosLivre[$key] = $value;
	}

	// Exécution de la requête
	$ajouterLivre->execute($infosLivre);
?>

<script type="text/javascript">
	// Redirection vers le nouveau livre
	window.location.href = 'livre.php?ISBN=<?= $infosLivre['ISBN'] ?>';
</script>