<?php
	require_once "includes/bdd.php";

	// Récupération des genres
	$genres = $bdd->prepare("SELECT * FROM biblio_genre");
	$genres->execute();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Ajouter livre</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<form method="POST" action="ajouterLivre.php">
			<!-- Formulaire -->
			<fieldset>
				<legend>Ajouter un livre</legend>
				<p>
					<label for="ISBN" class="required">ISBN</label>
					<input class="uk-input" type="text" name="ISBN" required>
				</p>
				<p>
					<label for="titre" class="required">Titre</label>
					<input class="uk-input" type="text" name="titre" required>
				</p>
				<p>
					<label for="auteur" class="required">Auteur</label>
					<input class="uk-input" type="text" name="auteur" required>
				</p>
				<p>
					<label for="genre" class="required">Genre</label>
					<select class="uk-select" name="genre" required>
						<?php
							// Affichage des genres
							while($genre = $genres->fetch()){
								echo "<option value=\"".$genre["id"]."\">".$genre["nom"]."</option>";
							}
						?>
					</select>
				</p>
				<p>
					<label for="parution">Date de parution</label>
					<input class="uk-input" type="date" name="parution">
				</p>
				<p>
					<label for="mc">Mots-clé</label>
					<input class="uk-input" type="text" name="mc">
				</p>
				<p>
					<label for="stock" class="required">Stock</label>
					<input class="uk-input" type="number" name="stock" required min="1">
				</p>
				<p>
					<label for="editeur">Éditeur</label>
					<input class="uk-input" type="text" name="editeur">
				</p>
				<p>
					<label for="collection">Collection</label>
					<input class="uk-input" type="text" name="collection">
				</p>
				<p>
					<label for="nbPages">Nombre de pages</label>
					<input class="uk-input" type="number" name="nbPages" min="0">
				</p>
				<p>
					<input class="uk-button uk-button-default" type="button" value="Annuler" onclick="history.back();">
					<input class="uk-button uk-button-primary" type="submit" value="Envoyer">
				</p>
			</fieldset>
		</form>
	</div>
</body>
</html>