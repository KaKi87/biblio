<?php
	require_once "includes/bdd.php";

	// Récupération du livre
	$livre = $bdd->prepare("SELECT * FROM biblio_livre INNER JOIN biblio_genre ON biblio_livre.genre = biblio_genre.id WHERE ISBN = :ISBN");
	$livre->execute([
		"ISBN" => $_GET["ISBN"]
	]);
	$livre = $livre->fetch();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | <?= $livre["titre"] ?></title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<ul class="nav">
			<li><a href=".">Accueil</a></li>
			<li><a href="#" onclick="history.back();">Retour</a></li>
		</ul>
		<hr>
		<h1><?= $livre["titre"] ?></h1>
		<p class="info">
			<span>ISBN :</span> <?= $livre["ISBN"] ?>
			<br>
			<span>Auteur :</span> <?= $livre["auteur"] ?>
			<br>
			<span>Genre :</span> <?= $livre["nom"] ?>
			<br>
			<span>Date de parution :</span> <?= $livre["parution"] ?>
			<br>
			<span>Mots-clé :</span> <?= $livre["mc"] ?>
			<br>
			<span>Éditeur :</span> <?= $livre["editeur"] ?>
			<br>
			<span>Collection :</span> <?= $livre["collection"] ?>
			<br>
			<span>Nombre de pages :</span> <?= $livre["nbPages"] ?>
			<br>
			<span>Stock :</span> <?= $livre["stock"] ?>
		</p>
		<hr>
		<p>
			<button class="book__edit uk-button uk-button-default">Éditer</button>
			<button class="book__delete uk-button uk-button-danger">Supprimer</button>
		</p>
	</div>
	<script type="text/javascript">
		document.querySelector('.book__edit').onclick = () => window.location.href = '<?= "editer-livre.php?ISBN=".$livre["ISBN"] ?>';
		document.querySelector('.book__delete').onclick = function(){
			UIkit.modal.prompt('Cette action est irréversible. Tapez « supprimer » pour confirmer.', '')
			.then(input => {
				if(input === 'supprimer')
					window.location.href = 'supprimerLivre.php?ISBN=<?= $livre['ISBN'] ?>';
				else
					UIkit.modal.alert('Action annulée.');
			});
		}
	</script>
</body>
</html>