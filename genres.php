<?php
require_once "includes/bdd.php";

// Récupération des genres
$genres = $bdd->prepare("SELECT * FROM biblio_genre");
$genres->execute();
$genres = $genres->fetchAll();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Genres</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
<div id="container">
	<ul id="nav">
		<!-- Navigation -->
		<a href=".">Retour à l'accueil</a>
	</ul>
	<hr>
	<h2>Liste des genres</h2>
	<p class="info"><?= count($genres); ?> genres enregistrés</p>
	<ul class="info">
		<?php
		foreach($genres as $genre){
			echo "<li><a href=\"genre.php?id=".$genre["id"]."\">".$genre["nom"]."</a></li>";
		}
		?>
	</ul>
</div>
</body>
</html>