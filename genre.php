<?php
require_once "includes/bdd.php";

// Récupération du genre
$genre = $bdd->prepare("SELECT * FROM biblio_genre WHERE id = :id");
$genre->execute([
	"id" => $_GET["id"] ?? $_POST["id"]
]);
$genre = $genre->fetch();

// Vérification si genre supprimable
$verif = $bdd->prepare("SELECT COUNT(ISBN) FROM biblio_livre WHERE genre LIKE :genre");
$verif->execute([ "genre" => $_GET["id"] ]);
$verif = $verif->fetch()[0];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | <?= $genre["nom"] ?></title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
<div id="container">
	<ul class="nav">
		<li><a href=".">Accueil</a></li>
		<li><a href="genres.php">Retour</a></li>
	</ul>
	<hr>
	<h1>Genre</h1>
	<p class="info">
		<span>Nom :</span> <?= $genre["nom"] ?>
	</p>
	<hr>
	<p>
		<button class="genre__edit uk-button uk-button-default">Éditer</button>
		<button class="genre__delete uk-button uk-button-default">Supprimer</button>
	</p>
	<script type="text/javascript">
		document.querySelector('.genre__edit').onclick = () => window.location.href="editer-genre.php?id=<?= $genre["id"] ?>";
		document.querySelector('.genre__delete').onclick = () => {
			if(<?= $verif ?> === 0)
				window.location.href="supprimerGenre.php?id=<?= $genre["id"] ?>";
			else
				UIkit.modal.alert(`Ce genre ne peut être supprimé tant qu'il contient des livres.`);
		}
	</script>
</body>
</html>