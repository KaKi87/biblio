<?php
	require_once "includes/bdd.php";

	// Requête SQL édition livre
	$editerLivre = $bdd->prepare("UPDATE biblio_livre SET ISBN = :ISBN, titre = :titre, auteur = :auteur, genre = :genre, parution = :parution, mc = :mc, stock = :stock, editeur = :editeur, collection = :collection, nbPages = :nbPages WHERE ISBN = :ancienISBN");

	// Tableau informations livre
	$infosLivre = [];

	// Stockage des infos dans tableau
	foreach($_POST as $key => $value){
		$infosLivre[$key] = $value;
	}

	// Exécution de la requête
	$editerLivre->execute($infosLivre);

	// Redirection vers page livre
	header("Location: livre.php?ISBN=".$infosLivre['ISBN']);