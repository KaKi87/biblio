<?php
	require_once "includes/bdd.php";

	// Requête SQL édition client
	$editerClient = $bdd->prepare("
		UPDATE biblio_client
		SET
			nom = :nom,
			prenom = :prenom,
			sexe = :sexe,
			adresse = :adresse,
			cp = :cp,
			ville = :ville,
			tel = :tel,
			mail = :mail
		WHERE code = :code
	");

	// Tableau informations client
	$infosClient = [];

	// Stockage des infos dans tableau
	foreach($_POST as $key => $value){
		$infosClient[$key] = $value;
	}

	// Exécution de la requête
	$editerClient->execute($infosClient);

	// Redirection vers page client
	header("Location: client.php?code=".$infosClient['code']);