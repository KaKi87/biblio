<?php
require_once "includes/bdd.php";

$stats = $bdd->prepare("SELECT livres, genres, clients, emprunts FROM biblio_stats WHERE id LIKE 0");
$stats->execute();
$stats = $stats->fetch();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Accueil</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<h2>Livres</h2>
		<ul class="nav">
			<li><a href="ajouter-livre.php">Ajouter un livre</a></li>
			<li><a href="livres.php">Gestion des livres</a></li>
		</ul>
		<h2>Genres</h2>
		<ul class="nav">
			<li><a href="ajouter-genre.php">Ajouter un genre</a></li>
			<li><a href="genres.php">Gérer les genres</a></li>
		</ul>
		<h2>Clients & emprunts</h2>
		<ul class="nav">
			<li><a href="ajouter-client.php">Ajouter un client</a></li>
			<li><a href="clients.php">Gérer les clients</a></li>
		</ul>
		<h2>Recherche</h2>
		<form method="POST" action="recherche.php">
			<fieldset>
				<legend>Livre</legend>
				<p>
					Recherche par :
					<label class="critere"><input type="radio" name="critere" value="titre" class="uk-radio" checked>Titre</label>
					<label class="critere"><input type="radio" name="critere" value="auteur" class="uk-radio">Auteur</label>
					<label class="critere"><input type="radio" name="critere" value="mc" class="uk-radio">Mot(s)-clé</label>
				</p>
				<p>
					<input type="text" name="recherche_livre" class="uk-input">
				</p>
				<p>
					<input type="submit" name="livre" value="Rechercher" class="uk-button uk-button-default">
				</p>
			</fieldset>
		</form>
		<form method="POST" action="recherche.php">
			<fieldset style="margin-top: 15px;">
				<legend>Client</legend>
				<p>
					Recherche par :
					<label class="critere"><input type="radio" name="critere" value="code" class="uk-radio">Code</label>
					<label class="critere"><input type="radio" name="critere" value="nom" class="uk-radio" checked>Nom</label>
				</p>
				<p>
					<input type="text" name="recherche_client" class="uk-input">
				</p>
				<p>
					<input type="submit" name="client" value="Rechercher" class="uk-button uk-button-default">
				</p>
			</fieldset>
		</form>
		<div id="resultats" <?= isset($_POST["resultats"]) ? '' : 'style="display: none;"' ?>>
			<hr>
			<h3>Résultats de recherche</h3>
			<?php
			if(isset($_POST["resultats"]))
				$resultats = json_decode($_POST["resultats"], true);
			?>
			<table <?= count($resultats) > 0 ? '' : 'style="display: none;"' ?>>
				<tr>
					<?php
					if($_POST['type'] == 'livre'){
						echo '<th>ISBN</th>
							  <th>Titre</th>
							  <th>Auteur</th>
							  <th>Genre</th>
							  <th>Parution</th>
							  <th>Stock</th>
							  <th>Editeur</th>
							  <th>Collection</th>
							  <th>Pages</th>';
					}
					else {
						echo '<th>Nom</th>
							  <th>Prénom</th>
							  <th>Sexe</th>
							  <th>Adresse</th>
							  <th>CP</th>
							  <th>Ville</th>
							  <th>Tel</th>
							  <th>Mail</th>';
					}
					?>
				</tr>
				<?php
				foreach($resultats as $resultat){
					echo '<tr>';
					foreach($resultat as $key => $value){
						if($key == 'code') continue;
						echo "<td>".$value."</td>";
					}
					if($_POST['type'] == 'livre'){
						echo "<td><a href=\"livre.php?ISBN=".$resultat["ISBN"]."\">Voir/éditer</a></td>";
					}
					else {
						echo "<td><a href=\"client.php?code=".$resultat["code"]."\">Voir/éditer</a></td>";
					}
					echo '</tr>';
				}
				?>
			</table>
			<p <?= count($resultats) == 0 ? '' : 'style="display: none;"' ?>>Aucun résultat</p>
		</div>
		<div id="stats">
			<h3>Statistiques</h3>
			<p>
				Livres : <?= $stats['livres'] ?>
				<br>
				Genres : <?= $stats['genres'] ?>
				<br>
				Clients : <?= $stats['clients'] ?>
				<br>
				Emprunts : <?= $stats['emprunts'] ?>
			</p>
		</div>
	</div>
</body>
</html>