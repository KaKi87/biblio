<?php
require_once "includes/bdd.php";

// Requête SQL ajout genre
$ajouterGenre = $bdd->prepare("INSERT INTO biblio_genre (nom) VALUES (:nom)");

// Tableau informations genre
$infosGenre = [];

// Stockage des infos dans tableau
foreach($_POST as $key => $value){
	$infosGenre[$key] = $value;
}

// Exécution de la requête
$ajouterGenre->execute($infosGenre);

// Récupération du nouveau genre
$nouveauGenre = $bdd->prepare("SELECT id FROM biblio_genre WHERE id = (SELECT MAX(id) FROM biblio_genre)");
$nouveauGenre->execute();
$nouveauGenre = $nouveauGenre->fetch();
?>

<script type="text/javascript">
	// Redirection vers le nouveau genre
	window.location.href = 'genre.php?id=<?= $nouveauGenre['id'] ?>';
</script>