<?php
require_once "includes/bdd.php";

// Suppression du genre
$genre = $bdd->prepare("DELETE FROM biblio_genre WHERE id = :id");
$genre->execute([
	"id" => $_GET["id"]
]);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Genre supprimé</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
<div id="container">
	<p>Genre supprimé.</p>
	<ul class="nav">
		<li><a href="index.php">Retour à l'accueil</a></li>
		<li><a href="genres.php">Retour à la gestion des genres</a></li>
	</ul>
</div>
</body>
</html>