<?php
	require_once "includes/bdd.php";

	// Récupération des clients
	$clients = $bdd->prepare("
      SELECT code, nom, prenom, ville, COUNT(biblio_emprunt.livre) AS emprunts
	  FROM biblio_client LEFT JOIN biblio_emprunt ON biblio_client.code = biblio_emprunt.client
      GROUP BY code;
	");
	$clients->execute();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Clients</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<ul id="nav">
			<!-- Navigation -->
			<a href=".">Retour à l'accueil</a>
		</ul>
		<hr>
		<h2>Liste des clients</h2>
		<table>
			<tr>
				<th>Code</th>
				<th>Nom</th>
				<th>Prénom</th>
				<th>Ville</th>
				<th>Emprunts</th>
			</tr>
			<?php
				while($client = $clients->fetch(PDO::FETCH_ASSOC)){
					echo "<tr>";
					foreach($client as $key => $info){
						echo "<td>".$info."</td>";
					}
					echo "<td><a href=\"client.php?code=".$client["code"]."\">Voir/éditer</a></td>";
					echo "</tr>";
				}
			?>
		</table>
	</div>
</body>
</html>