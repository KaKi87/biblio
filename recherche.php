<?php

require_once "includes/bdd.php";

// Type de recherche
$element_recherche = isset($_POST["livre"]) ? "livre" : "client";

// Requête de recherche
if($element_recherche == "livre"){
	$recherche = $bdd->prepare("
		SELECT ISBN, titre, auteur, biblio_genre.nom, parution, stock, editeur, collection, nbPages
		FROM biblio_".$element_recherche."
		INNER JOIN biblio_genre ON biblio_livre.genre = biblio_genre.id
		WHERE ".$_POST["critere"]." LIKE :recherche
	");
}
else {
	$recherche = $bdd->prepare("
		SELECT *
		FROM biblio_client
		WHERE ".$_POST["critere"]." LIKE :recherche
	");
}


// Paramètres de recherche
$recherche->execute([
	"recherche" => "%".$_POST["recherche_".$element_recherche]."%"
]);

$resultats = $recherche->fetchAll(PDO::FETCH_ASSOC);

?>

<form action="index.php" method="post">
	<input type="text" name="resultats">
	<input type="hidden" name="type" value="<?= $element_recherche ?>">
</form>

<script>
	document.querySelector('input').value = '<?= addslashes(json_encode($resultats)) ?>';
	document.querySelector('form').submit();
</script>