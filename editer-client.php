<?php
	require_once "includes/bdd.php";

	// Récupération du client
	$client = $bdd->prepare("
		SELECT * FROM biblio_client
		WHERE code = :code
	");
	$client->execute([
		"code" => $_GET["code"]
	]);
	$client = $client->fetch();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Éditer client</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<form method="POST" action="editerClient.php">
			<!-- Formulaire -->
			<fieldset>
				<legend>Éditer un client</legend>
				<p>
					<label for="nom" class="required">Nom</label>
					<input class="uk-input" type="text" name="nom" required value="<?= $client["nom"] ?>">
				</p>
				<p>
					<label for="prenom" class="required">Prénom</label>
					<input class="uk-input" type="text" name="prenom" required value="<?= $client["prenom"] ?>">
				</p>
				<p>
					<label for="sexe" class="required">Sexe</label>
					<select class="uk-select" name="sexe" required>
						<option value="M" <?= ($client["sexe"] == "M") ? " selected" : "" ?>>Masculin</option>
						<option value="F" <?= ($client["sexe"] == "F") ? " selected" : "" ?>>Féminin</option>
					</select>
				</p>
				<p>
					<label for="adresse" class="required">Adresse</label>
					<input class="uk-input" type="text" name="adresse" required value="<?= $client["adresse"] ?>">
				</p>
				<p>
					<label for="cp" class="required">Code postal</label>
					<input class="uk-input" type="text" name="cp" required pattern="[0-9]{5}" value="<?= $client["cp"] ?>">
				</p>
				<p>
					<label for="ville" class="required">Ville</label>
					<input class="uk-input" type="text" name="ville" required value="<?= $client["ville"] ?>">
				</p>
				<p>
					<label for="tel" class="required">Téléphone</label>
					<input class="uk-input" type="tel" name="tel" required pattern="^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" value="<?= $client["tel"] ?>">
				</p>
				<p>
					<label for="mail">Adresse mail</label>
					<input class="uk-input" type="email" name="mail" value="<?= $client["mail"] ?>">
				</p>
				<p>
					<input class="uk-button uk-button-default" type="button" value="Annuler" onclick="history.back();">
					<input class="uk-button uk-button-primary" type="submit" value="Envoyer">
				</p>
			</fieldset>
			<input type="hidden" name="code" value="<?= $client['code'] ?>">
		</form>
	</div>
</body>
</html>