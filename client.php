<?php
	require_once "includes/bdd.php";

	// Récupération du client
	$client = $bdd->prepare("SELECT * FROM biblio_client WHERE code = :code");
	$client->execute([
		"code" => $_GET["code"] ?? $_POST["code"]
	]);
	$client = $client->fetch();

	// Récupération des emprunts
	$emprunts = $bdd->prepare("
		SELECT livre, titre, auteur, biblio_genre.nom FROM biblio_emprunt
		INNER JOIN biblio_client ON biblio_emprunt.client = biblio_client.code
		INNER JOIN biblio_livre ON biblio_emprunt.livre = biblio_livre.ISBN
		INNER JOIN biblio_genre ON biblio_livre.genre = biblio_genre.id
		WHERE biblio_emprunt.client = :code
	");
	$emprunts->execute([
		"code" => $_GET["code"] ?? $_POST["code"]
	]);
	$emprunts = $emprunts->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | <?= $client["prenom"]." ".$client["nom"] ?></title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<ul class="nav">
			<li><a href=".">Accueil</a></li>
			<li><a href="clients.php">Retour</a></li>
		</ul>
		<hr>
		<h1><?= $client["prenom"]." ".$client["nom"] ?></h1>
		<p class="info">
			<span>Code :</span> <?= $client["code"] ?>
			<br>
			<span>Sexe :</span>
			<?php
				if($client["sexe"] == "M")
					echo "Masculin";
				else if($client["sexe"] == "F")
					echo "Féminin";
			?>
			<br>
			<span>Adresse :</span>
			<br>
			<?= $client["adresse"]."<br>".$client["cp"]." ".$client["ville"] ?>
			<br>
			<span>Téléphone :</span> <?= $client["tel"] ?>
			<br>
			<span>Mail :</span> <?= $client["mail"] ?>
		</p>
		<hr>
		<h2><?= count($emprunts) ?> emprunt<?= (count($emprunts) > 1) ? "s" : "" ?></h2>
		<table>
			<tr>
				<th>ISBN</th>
				<th>Titre</th>
				<th>Auteur</th>
				<th>Genre</th>
			</tr>
			<?php
				foreach($emprunts as $emprunt){
					echo "<tr>";
					foreach($emprunt as $key => $info){
						echo "<td>".$info."</td>";
					}
					echo "<td><a href=\"livre.php?ISBN=".$emprunt["livre"]."\">Voir le livre</a></td>";
					echo "</tr>";
				}
			?>
		</table>
		<hr>
		<form method="POST" action="emprunter-rendre.php">
			<fieldset>
				<legend>Emprunter / Rendre</legend>
				<p>
					<label for="ISBN">ISBN</label>
					<input class="uk-input" type="text" name="ISBN" required>
				</p>
				<p>
					<input class="uk-button uk-button-default" type="button" value="Vider" onclick="document.querySelector('input[name=ISBN]').value = '';">
					<input class="uk-button uk-button-primary" type="submit" value="Envoyer">
				</p>
                <?php
                if(isset($_POST["emprunte"]))
                    echo "<p>Emprunté : ".$_POST["emprunte"]."</p>";
                if(isset($_POST["rendu"]))
                    echo "<p>Rendu : ".$_POST["rendu"]."</p>";
                ?>
			</fieldset>
			<input type="hidden" name="client" value="<?= $client["code"] ?>">
		</form>
		<hr>
		<ul class="nav">
			<li><?= "<a href=\"editer-client.php?code=".$client["code"]."\">Éditer</a>" ?></li>
			<li><a href="#" id="supprimer">Supprimer</a></li>
		</ul>
	</div>
	<script type="text/javascript">
		document.querySelector('#supprimer').onclick = () => {
		    if(<?= count($emprunts) ?> === 0){
		    	UIkit.modal.prompt('Cette action est irréversible. Tapez « supprimer » pour confirmer.', '')
		    		.then(input => {
		    			if(input === 'supprimer')
		    				window.location.href = 'supprimerClient.php?code=<?= $client['code'] ?>';
		    			else
		    				UIkit.modal.alert('Action annulée.');
		    		});
            }
            else
				UIkit.modal.alert('Le client doit rendre ses livres avant de se désinscrire.');
		}
	</script>
</body>
</html>