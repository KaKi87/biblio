<?php
    require_once "includes/bdd.php";
?>

<!DOCTYPE html>
<html>
<head>
    <title>Biblio | Ajouter genre</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/uikit.min.css">
	<script defer src="js/uikit.min.js"></script>
	<script defer src="js/uikit-icons.min.js"></script>
</head>
<body>
<div id="container">
    <form method="POST" action="ajouterGenre.php">
        <!-- Formulaire -->
        <fieldset>
            <legend>Ajouter un genre</legend>
            <p>
                <label for="nom" class="required">Nom</label>
                <input class="uk-input" type="text" name="nom" required>
            </p>
            <p>
                <input class="uk-button uk-button-default" type="button" value="Annuler" onclick="history.back();">
                <input class="uk-button uk-button-primary" type="submit" value="Envoyer">
            </p>
        </fieldset>
    </form>
</div>
</body>
</html>