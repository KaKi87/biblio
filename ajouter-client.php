<?php
	require_once "includes/bdd.php";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Ajouter client</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<form method="POST" action="ajouterClient.php">
			<!-- Formulaire -->
			<fieldset>
				<legend>Ajouter un client</legend>
				<p>
					<label for="nom" class="required">Nom</label>
					<input class="uk-input" type="text" name="nom" required>
				</p>
				<p>
					<label for="prenom" class="required">Prénom</label>
					<input class="uk-input" type="text" name="prenom" required>
				</p>
				<p>
					<label for="sexe" class="required">Sexe</label>
					<select class="uk-select" name="sexe" required>
						<option value="M">Masculin</option>
						<option value="F">Féminin</option>
					</select>
				</p>
				<p>
					<label for="adresse" class="required">Adresse</label>
					<input class="uk-input" type="text" name="adresse" required>
				</p>
				<p>
					<label for="cp" class="required">Code postal</label>
					<input class="uk-input" type="text" name="cp" required pattern="[0-9]{5}">
				</p>
				<p>
					<label for="ville" class="required">Ville</label>
					<input class="uk-input" type="text" name="ville" required>
				</p>
				<p>
					<label for="tel" class="required">Téléphone</label>
					<input class="uk-input" type="tel" name="tel" required pattern="^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
				</p>
				<p>
					<label for="mail">Adresse mail</label>
					<input class="uk-input" type="email" name="mail">
				</p>
				<p>
					<input class="uk-button uk-button-default" type="button" value="Annuler" onclick="history.back();">
					<input class="uk-button uk-button-primary" type="submit" value="Envoyer">
				</p>
			</fieldset>
		</form>
	</div>
</body>
</html>