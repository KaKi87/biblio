DROP TABLE IF EXISTS biblio_emprunt;
DROP TABLE IF EXISTS biblio_client;
DROP TABLE IF EXISTS biblio_livre;
DROP TABLE IF EXISTS biblio_genre;
DROP TABLE IF EXISTS biblio_stats;

CREATE TABLE biblio_stats (
	id       INT PRIMARY KEY,
	livres   INT NOT NULL DEFAULT 0,
	genres   INT NOT NULL DEFAULT 0,
	clients  INT NOT NULL DEFAULT 0,
	emprunts INT NOT NULL DEFAULT 0
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8mb4
	COLLATE utf8mb4_unicode_ci;

CREATE TABLE biblio_client (
	code    INT(11)      PRIMARY KEY AUTO_INCREMENT,
	nom     VARCHAR(100) NOT NULL,
	prenom  VARCHAR(100) NOT NULL,
	sexe    CHAR(1)      NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	cp      VARCHAR(5)   NOT NULL,
	ville   VARCHAR(100) NOT NULL,
	tel     VARCHAR(10)  NOT NULL,
	mail    VARCHAR(100) DEFAULT NULL
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8mb4
	COLLATE utf8mb4_unicode_ci;

CREATE TABLE biblio_genre (
	id  INT(11)      PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(255) NOT NULL
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8mb4
	COLLATE utf8mb4_unicode_ci;

CREATE TABLE biblio_livre (
	ISBN       VARCHAR(100) PRIMARY KEY,
	titre      VARCHAR(100) NOT NULL,
	auteur     VARCHAR(100) NOT NULL,
	genre      INT(11)      NOT NULL,
	parution   DATE         DEFAULT NULL,
	mc         VARCHAR(100) DEFAULT NULL,
	stock      INT(11)      NOT NULL,
	editeur    VARCHAR(100) DEFAULT NULL,
	collection VARCHAR(100) DEFAULT NULL,
	nbPages    VARCHAR(100) DEFAULT NULL,
	CONSTRAINT fk_livre_genre FOREIGN KEY (genre) REFERENCES biblio_genre(id)
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8mb4
	COLLATE utf8mb4_unicode_ci;

CREATE TABLE biblio_emprunt (
	client INT(11)      NOT NULL,
	livre  VARCHAR(100) NOT NULL,
	date   DATETIME     NOT NULL,
	UNIQUE KEY client_livre (client, livre),
	CONSTRAINT fk_emprunt_client FOREIGN KEY (client) REFERENCES biblio_client(code),
	CONSTRAINT fk_emprunt_livre  FOREIGN KEY (livre)  REFERENCES biblio_livre(ISBN)
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8mb4
	COLLATE utf8mb4_unicode_ci;

INSERT INTO biblio_stats (id) VALUES (0);

DELIMITER $$
CREATE TRIGGER   biblio__livre_add AFTER INSERT ON   biblio_livre FOR EACH ROW BEGIN UPDATE biblio_stats SET   livres = livres   + 1 WHERE id LIKE 0; END $$
CREATE TRIGGER   biblio__livre_del AFTER DELETE ON   biblio_livre FOR EACH ROW BEGIN UPDATE biblio_stats SET   livres = livres   - 1 WHERE id LIKE 0; END $$
CREATE TRIGGER   biblio__genre_add AFTER INSERT ON   biblio_genre FOR EACH ROW BEGIN UPDATE biblio_stats SET   genres = genres   + 1 WHERE id LIKE 0; END $$
CREATE TRIGGER   biblio__genre_del AFTER DELETE ON   biblio_genre FOR EACH ROW BEGIN UPDATE biblio_stats SET   genres = genres   - 1 WHERE id LIKE 0; END $$
CREATE TRIGGER  biblio__client_add AFTER INSERT ON  biblio_client FOR EACH ROW BEGIN UPDATE biblio_stats SET  clients = clients  + 1 WHERE id LIKE 0; END $$
CREATE TRIGGER  biblio__client_del AFTER DELETE ON  biblio_client FOR EACH ROW BEGIN UPDATE biblio_stats SET  clients = clients  - 1 WHERE id LIKE 0; END $$
CREATE TRIGGER biblio__emprunt_add AFTER INSERT ON biblio_emprunt FOR EACH ROW BEGIN UPDATE biblio_stats SET emprunts = emprunts + 1 WHERE id LIKE 0; END $$
CREATE TRIGGER biblio__emprunt_del AFTER DELETE ON biblio_emprunt FOR EACH ROW BEGIN UPDATE biblio_stats SET emprunts = emprunts - 1 WHERE id LIKE 0; END $$
DELIMITER ;

INSERT INTO biblio_client (nom, prenom, sexe, adresse, cp, ville, tel, mail) VALUES
('Lemesle', 'Tiana',  'M', '15 Rue des S�urs de la Rivi�re', '87000', 'Limoges',          '0661388624', 'tiana.lemesle@protonmail.com'),
('Bouby',   'Claude', 'F', '2 Rue Des Etangs',               '87420', 'Saint Victurnien', '0953074349', 'claude.bouby.prof@gmail.com');

INSERT INTO biblio_genre (nom) VALUES
('Aventure'),
('Fantastique'),
('Science Fiction'),
('Horreur'),
('Biographie'),
('Fantasy'),
('Scolaire');

INSERT INTO biblio_livre (ISBN, titre, auteur, genre, parution, mc, stock, editeur, collection, nbPages) VALUES
( '2-07-051842-6', 'Harry Potter � l''�cole des sorciers',   'J. K. Rowling', 6, '1997-06-26', NULL,                              3, 'Gallimard Jeunesse', 'Folio Junior',         '306'),
( '2-07-052455-8', 'Harry Potter et la Chambre des secrets', 'J. K. Rowling', 6, '1999-03-23', 'harry potter, chambre, secrets', 15, 'Gallimard Jeunesse', 'Folio junior',         '364'),
('978-2401045682', 'Seuls avec tous: anthologie 2019-2020',  'Johan Faerber', 7, '2018-08-22', 'seul avec tous',                 10, 'Hatier',             'Classiques & Cie BTS', '168');

INSERT INTO biblio_emprunt VALUES
(1,  '2-07-051842-6', '2018-01-25 11:04:55'),
(1, '978-2401045682', '2019-01-31 14:16:35');