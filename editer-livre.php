<?php
	require_once "includes/bdd.php";

	// Récupération des genres
	$genres = $bdd->prepare("SELECT * FROM biblio_genre");
	$genres->execute();

	// Récupération du livre
	$livre = $bdd->prepare("SELECT * FROM biblio_livre INNER JOIN biblio_genre ON biblio_livre.genre = biblio_genre.id WHERE ISBN = :ISBN");
	$livre->execute([
		"ISBN" => $_GET["ISBN"]
	]);
	$livre = $livre->fetch();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Éditer livre</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<ul class="nav">
			<li><a href=".">Retour à l'accueil</a></li>
			<li><a href="livres.php">Retour à la liste des livres</a></li>
		</ul>
		<form method="POST" action="editerLivre.php">
			<!-- Formulaire -->
			<fieldset>
				<legend>Éditer un livre</legend>
				<p>
					<label for="ISBN" class="required">ISBN</label>
					<input class="uk-input" type="text" name="ISBN" required value="<?= $livre['ISBN'] ?>">
				</p>
				<p>
					<label for="titre" class="required">Titre</label>
					<input class="uk-input" type="text" name="titre" required value="<?= $livre['titre'] ?>">
				</p>
				<p>
					<label for="auteur" class="required">Auteur</label>
					<input class="uk-input" type="text" name="auteur" required value="<?= $livre['auteur'] ?>">
				</p>
				<p>
					<label for="genre" class="required">Genre</label>
					<select class="uk-select" name="genre" required>
						<?php
							// Affichage du genre
							while($genre = $genres->fetch()){
								echo "<option value=\"".$genre["id"]."\"".(($genre["id"] == $livre["genre"]) ? " selected" : "").">".$genre["nom"]."</option>";
							}
						?>
					</select>
				</p>
				<p>
					<label for="parution">Date de parution</label>
					<input class="uk-input" type="date" name="parution" value="<?= $livre['parution'] ?>">
				</p>
				<p>
					<label for="mc">Mots-clé</label>
					<input class="uk-input" type="text" name="mc" value="<?= $livre['mc'] ?>">
				</p>
				<p>
					<label for="stock" class="required">Stock</label>
					<input class="uk-input" type="number" name="stock" required min="1" value="<?= $livre['stock'] ?>">
				</p>
				<p>
					<label for="editeur">Éditeur</label>
					<input class="uk-input" type="text" name="editeur" value="<?= $livre['editeur'] ?>">
				</p>
				<p>
					<label for="collection">Collection</label>
					<input class="uk-input" type="text" name="collection" value="<?= $livre['collection'] ?>">
				</p>
				<p>
					<label for="nbPages">Nombre de pages</label>
					<input class="uk-input" type="number" name="nbPages" min="0" value="<?= $livre['nbPages'] ?>">
				</p>
				<p>
					<input class="uk-button uk-button-default" type="button" value="Annuler" onclick="history.back();">
					<input class="uk-button uk-button-primary" type="submit" value="Envoyer">
				</p>
			</fieldset>
			<input type="hidden" name="ancienISBN" value="<?= $livre['ISBN'] ?>">
		</form>
	</div>
</body>
</html>