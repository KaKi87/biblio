<?php
require_once "includes/bdd.php";

// Récupération du genre
$genre = $bdd->prepare("
		SELECT * FROM biblio_genre
		WHERE id = :id
");
$genre->execute([
	"id" => $_GET["id"]
]);
$genre = $genre->fetch();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Éditer genre</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
<div id="container">
	<form method="POST" action="editerGenre.php">
		<!-- Formulaire -->
		<fieldset>
			<legend>Éditer un genre</legend>
			<p>
				<label for="nom" class="required">Nom</label>
				<input class="uk-input" type="text" name="nom" required value="<?= $genre["nom"] ?>">
			</p>
			<p>
				<input class="uk-button uk-button-default" type="button" value="Annuler" onclick="history.back();">
				<input class="uk-button uk-button-primary" type="submit" value="Envoyer">
			</p>
			<input type="hidden" name="id" value="<?= $_GET["id"] ?>">
		</fieldset>
	</form>
</div>
</body>
</html>
