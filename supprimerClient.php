<?php
	require_once "includes/bdd.php";

	// Suppression du client
	$client = $bdd->prepare("DELETE FROM biblio_client WHERE code = :code");
	$client->execute([
		"code" => $_GET["code"]
	]);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Biblio | Client supprimé</title>
	<?php require_once "includes/head.php"; ?>
</head>
<body>
	<div id="container">
		<p>Client supprimé.</p>
		<ul class="nav">
			<li><a href="index.php">Retour à l'accueil</a></li>
			<li><a href="clients.php">Retour à la gestion des clients</a></li>
		</ul>
	</div>
</body>
</html>