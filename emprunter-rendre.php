<?php
	require_once "includes/bdd.php";

	// Informations requêtes
    $infos = [
        "client" => $_POST["client"],
        "ISBN" => $_POST["ISBN"]
    ];

	// Vérification si livre emprunté
	$emprunte = $bdd->prepare("SELECT COUNT(livre) AS emprunte FROM biblio_emprunt WHERE livre = :ISBN AND client = :client");
	$emprunte->execute($infos);
	$emprunte = intval($emprunte->fetchColumn());

	// Agir en fonction
	$req = $emprunte ? "DELETE FROM biblio_emprunt WHERE client = :client AND livre = :ISBN" : "INSERT INTO biblio_emprunt VALUES(:client, :ISBN, DEFAULT)";
	$stmt = $bdd->prepare($req);
	$stmt->execute($infos);

?>

<form method="POST" action="client.php">
	<!-- Code livre rendu ou emprunté, retour vers page client -->
    <input type="hidden" name="code" value="<?= $_POST["client"] ?>">
    <input type="hidden" name="<?= $emprunte ? "rendu" : "emprunte" ?>" value="<?= $_POST["ISBN"] ?>">
</form>

<script type="text/javascript">
	// Envoi du formulaire contenant les informations
    document.querySelector('form').submit();
</script>